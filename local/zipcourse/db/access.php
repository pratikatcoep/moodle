<?php
$capabilities = array(
    'local/zipcourse:addinstance' => array(
        'captype' => 'read',
        'contextlevel' => CONTEXT_MODULE,
        'archetypes' => array(
            'student'=>CAP_ALLOW,
	    'user'=>CAP_PROHIBIT
        ),
        'clonepermissionsfrom' => 'mod/data:viewentry'
    ),
    'local/zipcourse:view' => array(
	'captype' => 'read',
	'contextlevel' =>  CONTEXT_MODULE,
	'archetypes' => array(
		'student'=>CAP_ALLOW,
		'teacher'=>CAP_PROHIBIT
		)
	),
    'local/zipcourse:create' => array(
	'captype' => 'write',
	'contextlevel' => CONTEXT_MODULE,
	'archetypes' => array(
		'student'=>CAP_ALLOW,
		'user'=>CAP_PROHIBIT
		)
	)
);

